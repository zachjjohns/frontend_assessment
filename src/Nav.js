import React from "react";
import logo from "./assets/logo-white.png";

export default function Nav() {
  return (
    <nav>
      <div className="logo-container">
        <img src={logo} alt="logo" className="nav-logo" />
        <h1 className="org">StopDeath.org</h1>
      </div>
      <div className="links-div">
        <ul>
          <li>
            <a href="about.html">About</a>
          </li>
          <li>
            <a href="ipsum.html">Ipsum</a>
          </li>
          <li>
            <a href="dolor-sit-amet.html">Dolor Sit Amet</a>
          </li>
          <li>
            <a href="news.html">News</a>
          </li>
          <li>
            <a href="events.html">Events</a>
          </li>
        </ul>
        <button className="nav-button">Do Something</button>
      </div>
    </nav>
  );
}
