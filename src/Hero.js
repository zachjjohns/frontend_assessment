import React from "react";

export default function Hero() {
  return (
    <section className="hero-section">
      <div className="hero-div">
        <h2 className="hero-h2">Hero region messaging goes here wow!</h2>
        <p className="hero-p">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam.
        </p>
        <button className="hero-button">DO SOMETHING</button>
      </div>
    </section>
  );
}
