import React from "react";

export default function Featured() {
  return (
    <section className="featured-section">
      <h3>Featured Content</h3>
      <div className="featured-grid">
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
        <div className="featured-content">
          <p>Type</p>
          <p>Increase Prosperity With Positive Thinking</p>
        </div>
      </div>
    </section>
  );
}
