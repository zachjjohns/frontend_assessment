import React from "react";
import logo from "./assets/logo-white.png";
import facebook from "./assets/facebook.png";
import twitter from "./assets/twitter.png";
import instagram from "./assets/instagram.png";
import email from "./assets/email.png";

export default function Footer() {
  return (
    <footer>
      <div className="footer-left">
        <div className="logo-container">
          <img src={logo} alt="logo" className="nav-logo" />
          <p className="org">StopDeath.org</p>
        </div>
        <p className="copyright-p">
          ©2016 Lorem Ipsum Org. All Rights Reserved | A{" "}
          <a href="https://www.messageagency.com/">Message Agency</a> site
        </p>
      </div>
      <div className="footer-right">
        <div className="footer-nav">
          <ul>
            <li>
              <a href="press.html">Press</a>
            </li>
            <li>
              <a href="login.html">Login</a>
            </li>
            <li>
              <a href="contact.html">Contact</a>
            </li>
          </ul>
        </div>
        <div className="socials-div">
          <img src={facebook} alt="Facebook Icon" className="social-icon" />
          <img src={twitter} alt="Twitter Icon" className="social-icon" />
          <img src={instagram} alt="Instagram Icon" className="social-icon" />
          <img src={email} alt="Email Icon" className="social-icon" />
        </div>
      </div>
    </footer>
  );
}
