import "./App.scss";
import Nav from "./Nav";
import Hero from "./Hero";
import Subhero from "./Subhero";
import Impact from "./Impact";
import Featured from "./Featured";
import Footer from "./Footer";

function App() {
  return (
    <>
      <Nav />
      <Hero />
      <Subhero />
      <Impact />
      <Featured />
      <Footer />
    </>
  );
}

export default App;
