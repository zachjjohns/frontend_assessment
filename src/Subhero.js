import React from "react";
import handshake from "./assets/logo-blue.png";
import group from "./assets/group.png";
import care from "./assets/charity.png";

export default function Subhero() {
  return (
    <section className="subhero-section">
      <div className="subhero-div">
        <div className="value-div">
          <img src={handshake} alt="Handshake icon" className="value-icon" />
          <hr />
          <h3>Working Together</h3>
          <p>
            Lorem ipsum dolor sit met, consectetur Adipiscing elit, sed do
            eiusmod tempor Incididunt ut labore.
          </p>
        </div>
        <div className="value-div">
          <img src={group} alt="Community icon" className="value-icon" />
          <hr />
          <h3>Building Communities</h3>
          <p>
            Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur.
          </p>
        </div>
        <div className="value-div">
          <img
            src={care}
            alt="Icon with hand holding heart"
            className="value-icon"
          />
          <hr />
          <h3>Caring for the Earth</h3>
          <p>
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
            officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </div>
    </section>
  );
}
