# StopDeath.Org - Message Agency Assessment

A React app that acts as a prototype based on a provided image for mock non-profit "StopDeath.org".

## Contributor
[Zach Johnson](https://github.com/zachjjohns)

## Tech & Tools Used
* Built using React
* SASS/SCSS
* HTML/JS
* Adobe XD for wireframing / prototyping
* Mobile Responsiveness

## Deploy Link
[Click Here to view the deployed project via Surge](https://stop-death-org.surge.sh/)

## Screen Shots:

### Provided Mockup
![Provided mockup for StopDeath](https://i.imgur.com/KROEY3i.jpg)

### Wireframe (created on Adobe XD)
![Screen Shot of StopDeath Wireframe](https://i.imgur.com/cfr31tv.png)

## Decisions and Trade Offs
* This was completed as a take-home challenge for a Front End Developer position with [Message Agency](https://www.messageagency.com/). We were given a 24-hour timeframe to match a comp based on a single image.
* I started the project by creating a wireframe thru Adobe XD. I placed the image to trace over and create elements, logos, text, etc. I enjoy doing this process first because it allows me to compartmentalize the page and think about how components will be structured. I'm able to think ahead on how the page and CSS should be properly structured. It really takes away a lot of that flex box trial-and-error early on. 
* I implemented [SASS](https://sass-lang.com/) as a way to reduce repetitive code with different variables + a mixin for buttons (see `src/sass/_variables.scss`).
* For this specific project, I think I may have spent too much time trying to perfectly match the comp, which took away some time later on for things like responsiveness & accessibility. 

## Future Implementations
* Given more time, I'd implement the following:
    * React Router for page navigation, and building out HTML for pages like Login, About, etc
    * Error + Loading handling
    * Animations, scroll effects
    * More focus on responsiveness, accessibility, and load-times
